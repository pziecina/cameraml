package com.example.cameraml;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

public class ProductView extends View {
    public ProductView(Context context) {
        super(context);
    }

    public ProductView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ProductView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ProductView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
