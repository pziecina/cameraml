package com.example.cameraml;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionImageMetadata;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.List;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

public class CameraMainActivity extends AppCompatActivity {

    private static final String TAG = "CameraVideoRegister";

    private static final int REQUEST_CAMERA_PERMISSION_RESULT = 0;

    private static SparseIntArray ORIENTATIONS = new SparseIntArray();
    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    FirebaseVisionBarcodeDetectorOptions options =
            new FirebaseVisionBarcodeDetectorOptions.Builder()
                    .setBarcodeFormats(
                            FirebaseVisionBarcode.FORMAT_ALL_FORMATS)
                    .build();

//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
////    private int getRotationCompensation(String cameraId, Context context)
////            throws CameraAccessException {
////        // Get the device's current rotation relative to its "native" orientation.
////        // Then, from the ORIENTATIONS table, look up the angle the image must be
////        // rotated to compensate for the device's rotation.
////        int deviceRotation = getWindowManager().getDefaultDisplay().getRotation();
////        int rotationCompensation = ORIENTATIONS.get(deviceRotation);
////
////        // On most devices, the sensor orientation is 90 degrees, but for some
////        // devices it is 270 degrees. For devices with a sensor orientation of
////        // 270, rotate the image an additional 180 ((270 + 270) % 360) degrees.
////        CameraManager cameraManager = (CameraManager) context.getSystemService(CAMERA_SERVICE);
////        int sensorOrientation = cameraManager
////                .getCameraCharacteristics(cameraId)
////                .get(CameraCharacteristics.SENSOR_ORIENTATION);
////        rotationCompensation = (rotationCompensation + sensorOrientation + 270) % 360;
////
////        // Return the corresponding FirebaseVisionImageMetadata rotation value.
////        int result;
////        switch (rotationCompensation) {
////            case 0:
////                result = FirebaseVisionImageMetadata.ROTATION_0;
////                break;
////            case 90:
////                result = FirebaseVisionImageMetadata.ROTATION_90;
////                break;
////            case 180:
////                result = FirebaseVisionImageMetadata.ROTATION_180;
////                break;
////            case 270:
////                result = FirebaseVisionImageMetadata.ROTATION_270;
////                break;
////            default:
////                result = FirebaseVisionImageMetadata.ROTATION_0;
////                Log.e(TAG, "Bad rotation value: " + rotationCompensation);
////        }
////        return result;
////    }



    private ImageReader imageReader;

    private View barView;

    ImageReader.OnImageAvailableListener imageAvailableListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader imageReader) {
            //when a buffer is available from the camera
            //get the image
            Image image = imageReader.acquireLatestImage();
            Image.Plane[] planes = image.getPlanes();


            ByteBuffer sourceBuffer = image.getPlanes()[0].getBuffer();

            FirebaseVisionImageMetadata metadata = new FirebaseVisionImageMetadata.Builder()
                    .setWidth(mPreviewSize.getWidth())   // 480x360 is typically sufficient for
                    .setHeight(mPreviewSize.getHeight())  // image recognition
                    .setFormat(FirebaseVisionImageMetadata.IMAGE_FORMAT_YV12)
                    .setRotation(FirebaseVisionImageMetadata.ROTATION_90)
                    .build();

            images = FirebaseVisionImage.fromByteBuffer(sourceBuffer,metadata);

             detector = FirebaseVision.getInstance()
                    .getVisionBarcodeDetector(options);

            Task<List<FirebaseVisionBarcode>> result = detector.detectInImage(images)
                    .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                        @Override
                        public void onSuccess(List<FirebaseVisionBarcode> barcodes) {
                            for (FirebaseVisionBarcode barcode: barcodes) {
                                Rect bounds = barcode.getBoundingBox();
                                Point[] corners = barcode.getCornerPoints();

                                String rawValue = barcode.getRawValue();
                                System.out.println("Rozpoznaje" + rawValue + "corners " + corners.toString() + "bounds: " + bounds.toString());

                                if(!rawValue.isEmpty()) {

                                    barView = findViewById(R.id.settingsView);
                                    barView.layout(bounds.left,bounds.top * 133/100, bounds.right, bounds.bottom*133/100);
                                    barView.setBackgroundColor(Color.YELLOW);

//                                    View secondView = new View(CameraMainActivity.this);
//                                    secondView.setVisibility(View.VISIBLE);
//                                    secondView.setBackgroundColor(Color.argb(60,254,254,20));
//
//                                    ConstraintSet constraintSet = new ConstraintSet();
//                                    constraintSet.clone(constraintLayout);
//                                    constraintSet.connect(secondView.getId(),ConstraintSet.LEFT,constraintLayout.getId(),ConstraintSet.LEFT,0);
//                                    constraintSet.connect(secondView.getId(),ConstraintSet.TOP,constraintLayout.getId(),ConstraintSet.TOP,0);
//                                    constraintSet.applyTo(constraintLayout);
//
////                                    ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(100,40);
////                                    params.leftMargin = 100;
////                                    params.topMargin = 100;
//                                    constraintLayout.addView(secondView);





//                                    ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(bounds.width(), bounds.height());
//                                    secondView.setTop(bounds.top);
//                                    secondView.setLeft(bounds.left);
//                                    layoutParams.setMargins(bounds.left,bounds.top,bounds.right,bounds.bottom);
////                                    secondView.setLayoutParams(layoutParams);

                                }

                                int valueType = barcode.getValueType();
                                // See API reference for complete list of supported types
                                switch (valueType) {
                                    case FirebaseVisionBarcode.TYPE_WIFI:
                                        String ssid = barcode.getWifi().getSsid();
                                        String password = barcode.getWifi().getPassword();
                                        int type = barcode.getWifi().getEncryptionType();
                                        break;
                                    case FirebaseVisionBarcode.TYPE_URL:
                                        String title = barcode.getUrl().getTitle();
                                        String url = barcode.getUrl().getUrl();
                                        break;
                                }
                            }
                            // Task completed successfully
                            // ...

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            // Task failed with an exception
                            // ...
                        }
                    });


//            //copy it into a byte[]
//            byte[] outFrame = new byte[sourceBuffer.capacity()];
//            int outFrameNextIndex = 0;
//
//
//            outFrame[0] = planes[0].getBuffer();
//
//
//
//            //free the Image
            image.close();
        }
    };



    FirebaseVisionBarcodeDetector detector;
    FirebaseVisionImage images;
    private void firebaseRuner(){

    }

    private ConstraintLayout constraintLayout;

    // textura pobiera rozmiar i tu mozna dodac ustawienie obrazu z kamery
    private TextureView mTextureView;
    private TextureView.SurfaceTextureListener mSurfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            System.out.println("Surface takie parametry wchodza" + width + " : " + height);                               // do usuniecia
            setupCamera(width, height);
            connectCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int width, int height) {
            setupCamera(width, height);                                                                     // czy to tak moze byc?
            connectCamera();
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };

    private CameraDevice mCameraDevice;
    private CameraDevice.StateCallback mCameraDeviceStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            mCameraDevice = camera;
            startPreview();
        }

        @Override
        public void onDisconnected(CameraDevice camera) {
            camera.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(CameraDevice camera, int error) {
            camera.close();
            mCameraDevice = null;
        }
    };

    private HandlerThread mBackgroundHandlerThread;
    private Handler mBackgroundHandler;
    private String mCameraId;
    private Size mPreviewSize;
    private int mTotalRotation;

    private CaptureRequest.Builder mCaptureRequestBuilder;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        System.out.println("onCreate");                                  // usunąć

        setContentView(R.layout.activity_camera_main);
        constraintLayout = findViewById(R.id.constraintLayout);
        mTextureView = findViewById(R.id.textureView);
    }

    @Override
    protected void onResume() {
        super.onResume();

        startBackgroundThread();

        if(mTextureView.isAvailable()) {
            setupCamera(mTextureView.getWidth(), mTextureView.getHeight());
            connectCamera();
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

  /*  @Override
    protected void onStart() {
        super.onStart();

        System.out.println("onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        System.out.println("onRestart");
    }

    @Override
    protected void onStop() {
        System.out.println("onStop");
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        System.out.println("onDestroy");

        super.onDestroy();
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == REQUEST_CAMERA_PERMISSION_RESULT) {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not run without camera services", Toast.LENGTH_SHORT).show();
            }
            if(grantResults[1] != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(),
                        "Application will not have audio on record", Toast.LENGTH_SHORT).show();
            }
        }
//        if(requestCode == REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION_RESULT) {
//            if(grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                if(mIsRecording) {
//                    mIsRecording = true;
//                    mRecordImageButton.setImageResource(R.mipmap.ic_launcher);
//                }
//                Toast.makeText(this,
//                        "Permission successfully granted!", Toast.LENGTH_SHORT).show();
//            } else {
//                Toast.makeText(this,
//                        "App needs to save video to run", Toast.LENGTH_SHORT).show();
//            }
//        }
    }

    @Override
    protected void onPause() {
        System.out.println("onPause");
        closeCamera();

        stopBackgroundThread();

        super.onPause();
    }

    // To jest po to zeby usunac ten gorny label.

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus){
        super.onWindowFocusChanged(hasWindowFocus);
        View decorView = getWindow().getDecorView();
        if(hasWindowFocus) {
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    }

    private void setupCamera(int width, int height) {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            String cameraId = cameraManager.getCameraIdList()[0];
            CameraCharacteristics cameraCharacteristics = cameraManager.getCameraCharacteristics(cameraId);

            StreamConfigurationMap map = cameraCharacteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

            int deviceOrientation = getWindowManager().getDefaultDisplay().getRotation();
            mTotalRotation = sensorToDeviceRotation(cameraCharacteristics, deviceOrientation);
            boolean swapRotation = mTotalRotation == 0 || mTotalRotation == 180;
            int rotatedWidth = width;
            int rotatedHeight = height;
            if(swapRotation) {
                rotatedWidth = height;
                rotatedHeight = width;
            }

            System.out.println("Klasa dziwne strasznie: " + rotatedHeight + " rH " + rotatedWidth + " rW " + map.getOutputSizes(MediaRecorder.class)[0].toString());
            mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), rotatedWidth, rotatedHeight);

            imageReader = ImageReader.newInstance(mPreviewSize.getWidth(), mPreviewSize.getHeight(), ImageFormat.YV12, 3);
            imageReader.setOnImageAvailableListener(imageAvailableListener,mBackgroundHandler);

            mCameraId = cameraId;
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void connectCamera() {
        CameraManager cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler);
                } else {
                    if(shouldShowRequestPermissionRationale(android.Manifest.permission.CAMERA)) {
                        Toast.makeText(this,
                                "Video app required access to camera", Toast.LENGTH_SHORT).show();
                    }
                    requestPermissions(new String[] {android.Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO}, REQUEST_CAMERA_PERMISSION_RESULT);
                }

            } else {
                cameraManager.openCamera(mCameraId, mCameraDeviceStateCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void startPreview() {
        SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
        surfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        Surface previewSurface = new Surface(surfaceTexture);


        try {
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mCaptureRequestBuilder.addTarget(previewSurface);
            mCaptureRequestBuilder.addTarget(imageReader.getSurface());

            mCameraDevice.createCaptureSession(Arrays.asList(previewSurface,imageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {
                        @Override
                        public void onConfigured(CameraCaptureSession cameraCaptureSession) {
                            Log.d(TAG, "onConfigured: startPreview");
                            try {
                                cameraCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(),
                                        null, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(CameraCaptureSession session) {
                            Log.d(TAG, "onConfigureFailed: startPreview");

                        }
                    }, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void closeCamera() {
        if(mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }
    }

    private void startBackgroundThread() {
        mBackgroundHandlerThread = new HandlerThread("SamochodowyRejestratorWideo");
        mBackgroundHandlerThread.start();
        mBackgroundHandler = new Handler(mBackgroundHandlerThread.getLooper());
    }

    private void stopBackgroundThread() {
        mBackgroundHandlerThread.quitSafely();
        try {
            mBackgroundHandlerThread.join();
            mBackgroundHandlerThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Nad tym trzeba jeszcze popracować

    private static int sensorToDeviceRotation(CameraCharacteristics cameraCharacteristics, int rotation) {
        int sensorOrientation = cameraCharacteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
        rotation = ORIENTATIONS.get(rotation);
        return (rotation + sensorOrientation ) % 360;
    }

    private static Size chooseOptimalSize(Size[] choices, int width, int height) {
        for(Size option : choices) {
            if(option.getWidth() == option.getHeight() * height / width
                    && option.getWidth() <= width && option.getHeight() <= height) {
                return option;
            }else if(option.getHeight() == option.getWidth() * height / width
                    && option.getWidth() <= width && option.getHeight() <= height){
                return option;
            }
        }
        return choices[0];
    }  // dorobić obsługę gdy nie bedzie 16:9 lub 9:16
}